import React, { useState, FC } from 'react';
import './App.css';
import Numbers from './components/Numbers';
import MathOperations from './components/MathOperations';
import { Result } from './components/Result';
import Functions from './components/Functions';

const App : FC = () => {
  const [stack, setStack] = useState<string>("")
  const clickHandler = (number: string) => setStack(`${stack}${number}`)
  const clickOperation = (operation: string) => setStack(`${stack}${operation}`)
  // eslint-disable-next-line
  const clickEquals = () => {
    console.log(stack);
    setStack(eval(stack))
  }
  const clickClear = () => setStack('')
  const clickDelete = () => stack.length > 0 ? setStack(stack.substring(0, stack.length-1)) : null
  return (
    <main className="react-calculator">
      <Result value={stack}/>
      <Numbers clickHandler={clickHandler}/>
      <Functions onClear={clickClear} onDelete={clickDelete} />
      <MathOperations onClickOperation={clickOperation} onClickEquals={clickEquals}/>
    </main>
  );
}

export default App;
