import React, { FC } from 'react'
import Button from './Button'
import {ButtonClickHandler} from './Button/ButtonClickHandler'

type Prop = {
    onClear: ButtonClickHandler, 
    onDelete: ButtonClickHandler
}

const Functions : FC<Prop> = ({onClear, onDelete}) => {
    return (
        <section className="functions">
            <Button key="clear" type="button-long-text" text="clear" clickHandler={onClear}/>
            <Button key="delete" type="button-long-text" text="&larr;" clickHandler={onDelete}/>
        </section>
    )
}

export default Functions
