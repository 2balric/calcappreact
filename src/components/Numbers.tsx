import React, { FC } from 'react'
import Button from './Button'
import {ButtonClickHandler} from './Button/ButtonClickHandler'

const numbers = [7,8,9,4,5,6,1,2,3,0];
const renderButtons = (clickHandler : ButtonClickHandler) => 
                        (i : number) => 
                            <Button key={i} type="number" text={i.toString()} clickHandler={clickHandler} />

type Prop = {
    clickHandler: ButtonClickHandler
}
const Numbers: FC<Prop> = ({clickHandler}) => {
    return (
        <section className="numbers">
            {
                numbers.map( i => renderButtons(clickHandler)(i))
            }
        </section>
    )
}

export default Numbers
