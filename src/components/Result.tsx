import React, { FC } from 'react'

type Prop = {
    value: string
}
export const Result: FC<Prop> = ({value}) => {
    return (
        <div className="result">
            {value}
        </div>
    )
}
