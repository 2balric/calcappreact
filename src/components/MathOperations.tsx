import React, { FC } from 'react'
import Button from './Button'
import {ButtonClickHandler} from './Button/ButtonClickHandler'

type Prop = {
    onClickOperation: ButtonClickHandler,
    onClickEquals: ButtonClickHandler
}
const MathOperations : FC<Prop> = ({ onClickOperation, onClickEquals }) => {
    return (
        <section className="math-operations">
            <Button key="+" type="operation" text="+" clickHandler={onClickOperation}></Button>
            <Button key="-" type="operation" text="-" clickHandler={onClickOperation}></Button>
            <Button key="x" type="operation" text="*" clickHandler={onClickOperation}></Button>
            <Button key="/" type="operation" text="/" clickHandler={onClickOperation}></Button>
            <Button key="=" type="operation" text="=" clickHandler={onClickEquals}></Button>
        </section>
    )
}

export default MathOperations