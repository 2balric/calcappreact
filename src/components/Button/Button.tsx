import React, { FC } from 'react'
import './Button.css'
import { ButtonClickHandler } from './ButtonClickHandler'


type Prop = {
    type?: string,
    text: string,
    clickHandler: ButtonClickHandler
}
const Button: FC<Prop> = ({type, text, clickHandler}) => {
    return (
        <button className={type} onClick={() => clickHandler(text)}>
            {text}
        </button>
    )
}

export default Button